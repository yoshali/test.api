<?php


namespace App\Controller;

use App\Entity\Contact;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Project;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;

class ProjectsApi extends AbstractController
{
    const CODE_FILTER = 'code_filter';
    const MIN_AMOUNT_VALUE = 'min_amount';
    const MAX_AMOUNT_VALUE = 'max_amount';

    public function getSingleItem($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $project = $entityManager->getRepository(Project::class)->find($id);

        if (!$project) {
            return new Response('No project found for id', 404);
        }

        $serializer = $this->getSerializer();
        $jsonProject = $serializer->serialize($project, 'json');

        return new Response($jsonProject, 200);
    }

    public function create(Request $request, ValidatorInterface $validator)
    {
        $jsonProject = $request->getContent();

        $serializer = $this->getDeserializer();

        try {
            $projectsArray = $serializer->deserialize($jsonProject, 'App\Entity\Project[]', 'json');
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }

        //Если пытаемся добавить больше одного проекта за раз, возвращаем ошибку
        if (count($projectsArray) > 1) {
            return new Response('More than 1 object deserialized', 400);
        }

        $project = array_shift($projectsArray);

        $isValideResponse = $this->validateProject($project, $validator);
        if ($isValideResponse !== true) {
            return $isValideResponse;
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($project);
        $contacts = $project->getContacts();
        foreach ($contacts as $contact) {
            $entityManager->persist($contact);
        }
        $entityManager->flush();

        return $this->redirectToRoute('project_id', [
            'id' => $project->getId()
        ]);
    }

    public function update(Request $request, $id, ValidatorInterface $validator)
    {
        $jsonProperties = $request->getContent();
        $entityManager = $this->getDoctrine()->getManager();
        $project = $entityManager->getRepository(Project::class)->find($id);

        if (!$project) {
            return new Response('No project found for id', 404);
        }

        $serializer = $this->getDeserializer();
        try {
            $project = $serializer->deserialize(
                $jsonProperties,
                Project::class,
                'json',
                ['object_to_populate' => $project]
            );
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }

        $isValideResponse = $this->validateProject($project, $validator);
        if ($isValideResponse !== true) {
            return $isValideResponse;
        }


        $entityManager->persist($project);
        $entityManager->flush();

        return $this->redirectToRoute('project_id', [
            'id' => $project->getId()
        ]);
    }

    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $project = $entityManager->getRepository(Project::class)->find($id);

        if (!$project) {
            return new Response('No project found for id', 404);
        }

        $entityManager->remove($project);
        $entityManager->flush();

        return new Response(null, 200);
    }

    public function getList(Request $request)
    {
        $filters = [
            self::CODE_FILTER => [
                'param' => 'code',
                'andWherePart' => "p.code LIKE :code"
            ],
            self::MIN_AMOUNT_VALUE => [
                'param' => 'minAmount',
                'andWherePart' => 'p.budget >= :minAmount'
            ],
            self::MAX_AMOUNT_VALUE => [
                'param' => ':maxAmount',
                'andWherePart' => 'p.budget <= :maxAmount'
            ],
        ];

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder()
            ->select('p')
            ->from('App\Entity\Project', 'p');

        //Ищем в запросе фильтры из массива
        foreach ($filters as $filter => $values) {
            if ($request->query->get($filter)) {
                $qb->andWhere($values['andWherePart']);

                if (key($request->query->all()) == self::CODE_FILTER) {
                    $code = '%' . $request->query->get($filter) . '%';
                    $qb->setParameter($values['param'], $code);
                } else {
                    $qb->setParameter($values['param'], $request->query->get($filter));
                }
            }
        }
        $q = $qb->getQuery();
        $results = $q->execute();

        $serializer = $this->getSerializer();
        $jsonProjectsList = $serializer->serialize($results, 'json');

        return new Response($jsonProjectsList,200);
    }

    private function getDeserializer()
    {
        $reflectionExtractor = new ReflectionExtractor();
        $phpDocExtractor = new PhpDocExtractor();
        $propertyTypeExtractor = new PropertyInfoExtractor([$reflectionExtractor], [$phpDocExtractor, $reflectionExtractor], [$phpDocExtractor], [$reflectionExtractor], [$reflectionExtractor]);

        $normalizer = [
            new ObjectNormalizer(null, null, null, $propertyTypeExtractor),
            new ArrayDenormalizer()
        ];

        $encoders = [
            'json' => new JsonEncoder()
        ];

        return new Serializer($normalizer, $encoders);
    }

    private function getSerializer()
    {
        $encoder = new JsonEncoder();
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getName();
            },
        ];
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        return new Serializer([$normalizer], [$encoder]);
    }

    private function validateProject(Project $project, ValidatorInterface $validator)
    {
        $projectViolations = $validator->validate($project);
        if (count($projectViolations) > 0) {
            $errorsString = $projectViolations;

            return new Response($errorsString, 400);
        }
        $contacts = $project->getContacts();
        foreach ($contacts as $contact) {
            $contactViolations = $validator->validate($contact);
            if (count($contactViolations) > 0) {
                $errorsString = $contactViolations;

                return new Response($errorsString, 400);
            }
        }

        return true;
    }
}